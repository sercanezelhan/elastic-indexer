﻿using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticIndexer
{
    public class ElasticHelper : IHostedService
    {
        string _baseUri = Environment.GetEnvironmentVariable("ELASTIC_URL")?? "http://192.168.55.13:13030";
        public Task StartAsync(CancellationToken cancellationToken)
        {
            AddIndex("test");
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        test test = new test()
        {
            Id = 3,
            Name = "Gib Test"
        };

        public void AddIndex(string sensorId)
        {
            string uri = $"{_baseUri}/{sensorId}/_doc";
            IRestClient restClient = new RestClient();
            restClient.BaseUrl = new Uri(uri);
            IRestRequest restRequest = new RestRequest(Method.POST);
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddParameter(string.Empty, JsonConvert.SerializeObject(test), ParameterType.RequestBody);
            var response = restClient.Execute(restRequest);
        }
    }

    public class test
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
